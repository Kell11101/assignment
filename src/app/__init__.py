from flask import Flask
app = Flask(__name__)
app.config['SECRET_KEY'] = 'The-Most-random-string'

from app import routes
