class menuchoices:
    menuObj = None

    @classmethod
    def getSingleton(menu):
        if menu.menuObj == None:
            menu.menuObj = menuchoices()
        return menu.menuObj

    def __init__(self):
        self.currentChoices = []

    def getChoices(self):
        return self.currentChoices

    def addChoice(self, url, label):
        self.currentChoices.append((url, label))

    def removeChoice(self, label):
        toRemove = []
        #Finds all choices with the given label
        for i in range(len(self.currentChoices)):
            if self.currentChoices[i][1] == label:
                toRemove.append(self.currentChoices[i])
        #Removes all choices previously identified
        for i in toRemove:
            self.currentChoices.remove(i)

    def removeAll(self):
        self.currentChoices.clear()

    def isEmpty(self):
        return self.currentChoices == []
        
        
