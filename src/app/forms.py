from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField

class SelectPictureForm(FlaskForm):
    picture = SelectField('Picture', validate_choice=False)
    submit = SubmitField('Submit')
