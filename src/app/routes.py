from flask import render_template, redirect, send_file, request
from app import app
from app.forms import SelectPictureForm
import requests
from app.menuchoices import menuchoices

@app.route('/', methods=['GET','POST'])
@app.route('/picture', methods=['GET','POST'])
def picture():
    choices = menuchoices.getSingleton()
    if choices.isEmpty():
        requests.get('http://' + request.host + '/admin?init=True')
        
    form = SelectPictureForm()
    form.picture.choices = choices.getChoices()
    
    if form.validate_on_submit():
        r = requests.get(form.picture.data)
        #Potential XSS vulnerability as page doesn't get escaped
        #to allow the inserted html to display properly
        #However the XSS vulerability on exists while there is
        #an SSRF vulerability
        return render_template('picture.html', form=form, page=r.text)
    return render_template('picture.html',form=form)

#Both /resource/ pages should be moved to a second server to better
#simulate the SSRF vulnerability as these are the resources being
#requested by this server
@app.route('/resource/image/<resource>')
def getImage(resource):
    return send_file('static/' + resource)

@app.route('/resource/<resource>')
def getHTML(resource):
    imageURL = 'http://' + request.host + '/resource/image/' + resource + '.jpg'
    return render_template('picturedisplay.html', image=imageURL)

@app.route('/admin')
def admin():
    #This if statement determines whether access to the admin page is permited
    #The current set up is to simulate an environment where all users on
    #the LAN are trusted. As this server will only be hosted on LAN however,
    #the admin page is restricted to only be avaliable to users
    #on the host machine
    if(request.remote_addr == request.host.split(':')[0]):
        menu = menuchoices.getSingleton()
        #Handles query parameters
        if request.args:
            args = request.args
            #Initialises/resets choices
            #Is done during a request to easily get host ip
            if 'init' in args and args['init'] == 'True':
                url = 'http://' + request.host + '/resource'
                menu.removeAll()
                menu.addChoice(url + '/catone','Cat One')
                menu.addChoice(url + '/cattwo', 'Cat Two')
                menu.addChoice(url + '/catthree', 'Cat Three')
            #Deletes the choice with label delete
            if 'delete' in args:
                menu.removeChoice(args['delete'])
            #Adds the choice with label add and url url
            if 'add' in args and 'url' in args:
                menu.addChoice(args['url'], args['add'])
        return render_template('admin.html')
    return render_template('admindeny.html')
