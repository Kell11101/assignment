SET UP:
The current version requires:
python
flask
flask-wtf
requests

flask, flask-wtf and requests can be installed with: pip install <module_name>

To run the application set the environment variable FLASK_APP to WebApp.py with
set FLASK_APP=WebApp.py
(use export on linux (TBH I haven't checked that export works on linux, that's just what the tutorial says))

afterwards the appliaction can be run with:
flask run
or:
flask run --host=0.0.0.0

The former will run the application on localhost and so only accessabile from that machine.
The later will allow the application to be accessed from other machines on the LAN.
(I don't know enough about networking to know the difference between setting the host to 0.0.0.0 
and setting the host to your machine's ip. Most of my testing has used 0.0.0.0)

Once running, the application can be accessed at http://<ip_address>:5000/
(ip_address is either localhost or your machine's ip)
Note that you may need to disable the firewall on the host machine before the appliaction can be accessed by other machines

PAGES:
There are currently two pages intended to be accessed by a person:
http://<ip_address>:5000/ (also accessed at http://<ip_address>:5000/picture both pages will be refered to as /picture)
http://<ip_address>:5000/admin

The /admin page can be used to add, delete or reset the available drop down menu option using url query parameters.
Adding options:
Options can be added using add=<label>&url=<url>
replacing <label> with the display label and <url> with the desired url
Deleting:
Options can be deleted using delete=<label>
replacing <delete> with the label as show in the drop down menu
Reset:
The available options can be reset to the default using init=True
This is also used any time /picture is requested and the list of choices is empty 
(including the first request unless an option has been added through /admin)

Note that the /admin page will display an accessed denyed message if the user attempts to access it from a computer
other than the one that is hosting the application

There is are also two sets of pages used internaly for retrieving info
These will later be moved to a second server to make the simulation more authentic
http://<ip_address>:5000/resource/image/<file_name>
http://<ip_address>:5000/resource/<image_file_name>

The first sends the image with the file name <file_name> stored in the static folder
The second returns html which includes a url reference to the corresponding file

The page /picture renders html retrieved from the url selected by the drop down menu (The user only sees the label, behind
the scenes the menu is selecting a url). The page has been implemented in this way to make the SSRF exploit 
easier to understand and explain.

SSRF:
The SSRF vulnerability occures in the POST request made with the menu on the /picture page.
By intercepting this request the value for picture can be changed from the request for one of the cat pages to /admin
E.g.
	picture=http%3A%2F%2F192.168.1.81%3A5000%2Fresource%2Fcatone

changed to
	picture=http%3A%2F%2F192.168.1.81%3A5000%2Fadmin

doing so will display the admin page regardless of the machine the user is using rather than the accessed denyed message